package cc.w7.demoandroid;

import android.app.Activity;
import android.os.Bundle;
import android.w7.cc.demoandroid.R;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.Volley;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

public class ArticleActivity extends Activity {

    //Volley的全局请求队列
    public static RequestQueue sRequestQueue;

    private ListView list;
    private TextView text;
    private JSONObject config;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_article);
        this.list = this.findViewById(R.id.list);
        this.text = this.findViewById(R.id.url);
        this.text.setText(Url.url("index"));
        sRequestQueue = Volley.newRequestQueue(this);
        try {
            this.init();
        } catch (Exception e) {
            e.printStackTrace();

        }


    }


    private void init() {

        sRequestQueue.add(this.articleList());
        sRequestQueue.start();
    }

    /**
     *  文章列表
     */
    private JsonObjectRequest articleList() {
        final RequestQueue queue = sRequestQueue;
        return new JsonObjectRequest(Url.url("index"), new Response.Listener<JSONObject>() {
            @Override
            public void onResponse(JSONObject jsonObject) {
                try {
                    JSONArray array = jsonObject.getJSONArray("data");
                    ArticleActivity.this.list.setAdapter(new ListAdapter(array, queue));

                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                Toast.makeText(ArticleActivity.this.getBaseContext(), "获取文章列表失败", Toast.LENGTH_LONG);
            }
        });
    }
}
