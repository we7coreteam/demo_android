package cc.w7.demoandroid;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.pm.ApplicationInfo;
import android.database.DataSetObserver;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ListView;
import android.widget.Toast;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonArrayRequest;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.Volley;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.util.zip.ZipFile;

public class MainActivity extends Activity {

    //Volley的全局请求队列
    public static RequestQueue sRequestQueue;

    private ListView list;

    private JSONObject config;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
    }


    public void setting(View view) {
        Intent starter = new Intent(this, SettingActivity.class);
        this.startActivity(starter);
    }

    public void list(View view) {
        Intent starter = new Intent(this, ArticleActivity.class);
        this.startActivity(starter);
    }


}
