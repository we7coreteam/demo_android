package cc.w7.demoandroid;

import android.app.Activity;
import android.content.Context;
import android.content.pm.ApplicationInfo;
import android.net.Uri;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;

import android.widget.TextView;
import android.widget.Toast;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.util.zip.ZipFile;

/**
 *  建议开发者配置读取放到全局Application中
 */
public class SettingActivity extends Activity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_setting);

        TextView text = (TextView) findViewById(R.id.textView2);
        String comment = readApk();
        comment = comment == null ? "读取失败" : comment;
        text.setText(comment);
        try {
            Url.url = Url.commentToUrl(comment);
            text.setText(comment+ "url:"+Url.url);
        } catch (Exception e) {
            e.printStackTrace();
            Log.e("url", Url.url, e);
        }
    }

    private static String getApkPath(Context context) {
        String apkPath = null;
        try {
            ApplicationInfo applicationInfo = context.getApplicationInfo();
            if (applicationInfo == null) {
                return null;
            } else {
                apkPath = applicationInfo.sourceDir;
            }
        } catch (Throwable e) {
            e.printStackTrace();
        }
        return apkPath;
    }


    private String readApk()  {

        String path = getApkPath(this.getApplicationContext());
        ZipFile file = null;
        try {
            file = new ZipFile(path);
            String comment = file.getComment();
            return comment;
        } catch (IOException e) {
            e.printStackTrace();
        }
        return null;

    }
}
