package cc.w7.demoandroid;

import android.net.Uri;
import android.util.Log;

import org.json.JSONException;
import org.json.JSONObject;

import java.net.URI;
import java.net.URL;

public class Url {

    public static String url = "http://test.cc/app/index.php?i=47&j=0&version=1.1&m=we7_android&do=index&c=entry&a=phoneapp";

    public static String url(String $do) {
        return url+"&do="+$do;
    }

    /**
     * var siteinfo = {
     "uniacid": "14", "acid": "14", "multiid": "0", "version": "1.02", "m":"weisrc_businesscenter",
     "siteroot": "https://b.wxq0319.cn/app/index.php", "design_method": "3" }
     module.exports = siteinfo;
     * @param comment
     * @return
     * @throws JSONException
     */
    public static String commentToUrl(String comment) throws JSONException {
        JSONObject obj = new JSONObject(comment);

        int uniacid = obj.getInt("uniacid");
        int acid = 0;
        try{
            acid = obj.getInt("acid");
        }catch (Exception $e) {
            Log.e("url", "error acid", $e);
        }

        String version = obj.getString("version");
        String siteroot = obj.getString("siteroot");
        String m = obj.getString("m");

        String result =  siteroot+"?c=entry&a=phoneapp&i="+uniacid+"&j="+acid+"&version="+version+"&m="+m;

        return result;

    }
}
