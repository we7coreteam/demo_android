package cc.w7.demoandroid;

import android.graphics.Bitmap;
import android.support.v4.util.LruCache;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.w7.cc.demoandroid.R;
import android.widget.BaseAdapter;
import android.widget.TextView;

import com.android.volley.RequestQueue;
import com.android.volley.toolbox.ImageLoader;
import com.android.volley.toolbox.NetworkImageView;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

public class ListAdapter extends BaseAdapter {

    private JSONArray array;


    private ImageLoader imageLoader;
    public ListAdapter(JSONArray array, RequestQueue requestQueue) {
        this.array = array;
        this.imageLoader = new ImageLoader(requestQueue, new BitmapCache());

    }
    @Override
    public int getCount() {
        return array.length();
    }

    @Override
    public Object getItem(int position) {
        try {
            return array.get(position);
        } catch (JSONException e) {
            e.printStackTrace();
        }
        return null;
    }

    @Override
    public long getItemId(int position) {
        return 0;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        LayoutInflater inflater = LayoutInflater.from(parent.getContext());
        View view = convertView;

        if (convertView==null) {
            //因为getView()返回的对象，adapter会自动赋给ListView
            view = inflater.inflate(R.layout.listitem, null);
        }

        try {
            TextView tv1 = (TextView) view.findViewById(R.id.title);//
            NetworkImageView imageView = (NetworkImageView) view.findViewById(R.id.image);
            JSONObject obj = (JSONObject) this.getItem(position);
            String url = obj.getString("thumb");
            tv1.setText(obj.getString("title"));//设置参数
            TextView info = (TextView)view.findViewById(R.id.info);
            info.setText(obj.getString("description"));
            try {
                imageView.setDefaultImageResId(R.mipmap.ic_launcher);
                imageView.setErrorImageResId(R.mipmap.ic_launcher);
                imageView.setImageUrl(url, this.imageLoader);
            }catch (Exception e) {
                Log.d("imageurl", url);
            }



        } catch (JSONException e) {
            e.printStackTrace();
        }

        return view;
    }
}
//  链接：https://www.jianshu.com/p/d9072d3d00be
class BitmapCache implements ImageLoader.ImageCache {
    //LruCache对象
    private LruCache<String, Bitmap> lruCache ;
    //设置最大缓存为10Mb，大于这个值会启动自动回收
    private int max = 10*1024*1024;

    public BitmapCache(){
        //初始化 LruCache
        lruCache = new LruCache<String, Bitmap>(max){
            @Override
            protected int sizeOf(String key, Bitmap value) {
                return value.getRowBytes()*value.getHeight();
            }
        };
    }
    @Override
    public Bitmap getBitmap(String url) {
        return lruCache.get(url);
    }
    @Override
    public void putBitmap(String url, Bitmap bitmap) {
        lruCache.put(url, bitmap);
    }
}

